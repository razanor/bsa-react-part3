import React from 'react';
import PropTypes from 'prop-types';
import { Segment } from 'semantic-ui-react';

const Header = ( { participants, messages, mostRecentDate }) => {
    return (
        <Segment className="header">
            <ul className="left-info">
                <li>My chat</li>
                <li>{participants} {' '} participants</li>
                <li>{messages} {' '} messages</li>
            </ul>
            <ul className="right-info">
                <li>Last message {' '} {mostRecentDate}</li>
            </ul>
        </Segment>
    );
};

Header.propTypes = {
    participants: PropTypes.number.isRequired,
    messages: PropTypes.number.isRequired,
    mostRecentDate: PropTypes.string.isRequired
}

export default Header;