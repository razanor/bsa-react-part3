import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import EditMessage from '../../components/EditMessage';
import { deleteMessage } from '../../containers/Chat/actions';
import { Icon, Feed, Button } from 'semantic-ui-react';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            isLiked: false,
            isHover: false
        }
        this.openModal = this.handler.bind(this); 
    }

    handler(isModalOpen) {
        this.setState({
            isModalOpen
        })
    }

    hanldeDeletePost() {
        const { message, deleteMessage } = this.props;
        deleteMessage(message.id);
    }

    render () {
        const { message, currentUser } = this.props;
        const { isModalOpen, isLiked, isHover } = this.state;
        const {
            user,
            avatar,
            created_at,
            message: mes,
        } = message;

        const date = moment(created_at).fromNow();

        return (
            <div>
                <EditMessage 
                    isModalOpen={isModalOpen} 
                    message={message}
                    openModal={this.openModal} 
                />
                <Feed 
                    onMouseEnter={() => this.setState({ isHover: true })} 
                    onMouseLeave={() => this.setState({ isHover: false })} 
                >
                    <Feed.Event>
                        {avatar &&
                            <Feed.Label>
                                <img src={avatar} alt="avatar" />
                            </Feed.Label>
                        }
                        <Feed.Content>
                            <Feed.Summary>
                                <Feed.User>{user}</Feed.User>
                                <Feed.Date>{date}</Feed.Date>
                                { user === currentUser &&
                                    <Button 
                                        className='actionsBtn' 
                                        icon 
                                        onClick={() => this.hanldeDeletePost()}
                                    >
                                        <Icon name='delete' color='red' />
                                    </Button>   
                                }
                            </Feed.Summary>
                            <Feed.Extra text>
                            { mes }
                            </Feed.Extra>
                            <Feed.Meta>
                                <Feed.Like onClick={() => this.setState({ isLiked: !isLiked })}>
                                    <Icon name='like' />
                                    {isLiked && user !== currentUser ? 1 : 0} {' '} likes
                                </Feed.Like>
                                { ((user === currentUser) && isHover) &&
                                    <Button 
                                        className='actionsBtn' 
                                        icon
                                        onClick={() => this.setState({ isModalOpen: true })}
                                    >
                                        <Icon name='edit' color='green' />
                                    </Button>   
                                }
                            </Feed.Meta>
                        </Feed.Content>
                    </Feed.Event>
                </Feed>
            </div>
        );
    }
}

Message.propTypes = {
    deleteMessage: PropTypes.func.isRequired,
    message: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    currentUser: state.auth.user.name
});

export default connect(mapStateToProps, { deleteMessage } )(Message);