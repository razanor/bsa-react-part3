import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { editMessage } from '../../containers/Chat/actions';
import { Button, Modal, Form, TextArea } from 'semantic-ui-react';

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageBody: this.props.message.message
        } 
    }

    handleCloseModal() {
        const { message } = this.props;
        this.props.openModal(false);
        this.setState({ messageBody: message.message})
    }

    handleEditPost() {
        const { message, openModal, editMessage } = this.props;
        const { messageBody } = this.state;
        if (messageBody.trim().length > 0) {
            const newMessage = {
                id: message.id,
                user: message.user,
                avatar: message.avatar,
                message: messageBody,
                created_at: message.created_at,
                marker_read: false
            }
            editMessage(newMessage);
            openModal(false);
        }
    }

    render () {
        const { isModalOpen } = this.props;

        return (
            <Modal 
                closeIcon 
                dimmer="blurring" 
                centered={false} 
                open={isModalOpen} 
                onClose={() => this.handleCloseModal()}
                className='modalEdit'
            >
                <Modal.Header>Edit message</Modal.Header>
                <Modal.Content>
                <Form onSubmit={() => this.handleEditPost()}>
                    <Form.Group inline>
                        <Form.Field 
                            className="message-input" 
                            control={TextArea}  
                            placeholder='Message'
                            onChange={ev => this.setState({ messageBody: ev.target.value })}
                            value={this.state.messageBody} 
                        />
                        <Button color='vk'>Edit</Button>
                    </Form.Group>
                </Form>
                </Modal.Content>
            </Modal>    
        );
    }
}

EditMessage.propTypes = {
    editMessage: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    message: PropTypes.object.isRequired
}

export default connect(null, { editMessage } )(EditMessage);