import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addMessage } from '../../containers/Chat/actions';
import uuid from 'uuid';
import { Form, TextArea, Button, Icon } from 'semantic-ui-react';
import defaultProperty from '../../shared/default/data';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messageBody: ''
        }
    }

    handleAddPost() {
        const { messageBody } = this.state;
        const { addMessage, userName } = this.props;
        if (messageBody.trim().length > 0) {
            const newMessage = {
                id: uuid(),
                user: userName,
                avatar: defaultProperty.profileImage,
                created_at: Date.now(),
                message: this.state.messageBody,
                marked_read: false
            }
            addMessage(newMessage);
            this.setState({ messageBody: '' });
        }
    }

    render() {
        return (
            <Form onSubmit={() => this.handleAddPost()}>
                <Form.Group inline>
                    <Form.Field 
                        className="message-input" 
                        control={TextArea}  
                        placeholder='Message'
                        onChange={ev => this.setState({ messageBody: ev.target.value })}
                        value={this.state.messageBody} 
                    />
                    <Button labelPosition='left' icon color='vk'>Send
                        <Icon name='hand point up outline' />
                    </Button>
                </Form.Group>
            </Form>
        );
    } 
}

MessageInput.propTypes = {
    addMessage: PropTypes.func.isRequired,
    userName: PropTypes.string.isRequired
}

const mapStateToProps = state => ({
    userName: state.auth.user.name
});

export default connect(mapStateToProps, { addMessage } )(MessageInput);