import React from 'react';
import PropTypes from 'prop-types';
import { Image, Container, Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { logout } from '../../containers/Login/actions';
import { Link } from 'react-router-dom';

const Logo = ({ logout }) => {
    return (
        <Container className='mainHeader'>
            <Link to='/chat'><Image size='small' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3A6CzwqxUzweNAv6CO8AumK3DYqSbTqDQVdOcixmDMxy46knZTA' /></Link>
            <Button basic icon type="button" onClick={logout}>
                <Icon color="red" name="log out" size="big" />
            </Button>
        </Container>
    );
};

Logo.propTypes = {
    logout: PropTypes.func.isRequired
}

export default connect(null, { logout })(Logo);