import {
    SET_IS_LOADING, 
    SET_ALL_MESSAGES,
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SET_ALL_MESSAGES_SUCCESS
} from './actionTypes';


export const setAllMessages = messages => ({
    type: SET_ALL_MESSAGES_SUCCESS,
    messages
});

export const setIsLoading = isLoading => ({
    type: SET_IS_LOADING,
    isLoading
});

export const addMessage = message => ({
    type: ADD_MESSAGE,
    message
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    id
});

export const editMessage = message => ({
    type: EDIT_MESSAGE,
    message
});

export const fetchMessages = () => ({
    type: SET_ALL_MESSAGES
});
