import {
    SET_ALL_MESSAGES_SUCCESS,
    SET_IS_LOADING
} from './actionTypes';

const initialState = {
    isLoading: true,
    messages: [],
}

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case SET_ALL_MESSAGES_SUCCESS: {
            return {
                ...state,
                messages: action.messages.reverse() // Reverse needed to keep scrollbar at the bottom when API call is performed(its using flex-direction: column-reverse in order to do it)
            }
        }
        default:
            return state;
    }
}