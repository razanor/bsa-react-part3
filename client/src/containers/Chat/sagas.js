import axios from 'axios';
import api from '../../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { SET_ALL_MESSAGES, ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from './actionTypes';
import { setAllMessages as actionSetAllMessages, setIsLoading } from './actions';

// Watcher saga -> actions -> worker saga

function* setAllMessages() {
    try {
        yield put(setIsLoading(true));
        const messages = yield call(axios.get, `${api.url}/messages`);
        yield put(actionSetAllMessages(messages.data))
        yield put(setIsLoading(false));
    } catch (error) {
        console.log('setAllMessages error:', error.message)
    }
}

function* watchSetAllMessages() {
    yield takeEvery(SET_ALL_MESSAGES, setAllMessages);
}

function* addMessage(action) {
	try {
		yield call(axios.post, `${api.url}/messages`, action.message);
		yield put({ type: SET_ALL_MESSAGES });
	} catch (error) {
		console.log('createMessage error:', error.message);
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

function* editMessage(action) {
	const id = action.message.id;
	try {
		yield call(axios.put, `${api.url}/messages/${id}`, action.message);
		yield put({ type: SET_ALL_MESSAGES });
	} catch (error) {
		console.log('updateMessage error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(EDIT_MESSAGE, editMessage)
}

function* deleteMessage(action) {
	try {
		yield call(axios.delete, `${api.url}/messages/${action.id}`);
		yield put({ type: SET_ALL_MESSAGES })
	} catch (error) {
		console.log('deleteMessage Error:', error.message);
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* chatSaga() {
    yield all([
        watchSetAllMessages(),
        watchAddMessage(),
        watchUpdateMessage(),
        watchDeleteMessage()
    ])
}