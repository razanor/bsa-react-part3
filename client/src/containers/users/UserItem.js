import React from 'react';

const UserItem = ({ id, name, surname, email, onEdit, onDelete }) => {
    return (
        <div className="container list-group-item" style={{ margin: "6px" }}>
            <div className="row">
                <div className="col-8">
                    <span className="badge badge-secondary float-left" style={{ fontSize: "1.5em", margin: "2px" }}>{name} {surname}</span>
                    <span className="badge badge-info" style={{ fontSize: "1.5em", margin: "2px" }}>{email}</span>
                </div>
                <div className="col-4 btn-group">
                    <button className="btn btn-outline-primary" onClick={(e) => onEdit(id)}> Edit </button>
                    <button className="btn btn-outline-dark" onClick={(e) => onDelete(id)}> Delete </button>
                </div>
            </div>
        </div>
    );
};

export default UserItem;