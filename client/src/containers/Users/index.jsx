import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserItem from './UserItem';
import * as actions from './actions';
import PropTypes from 'prop-types';
import Logo from '../../components/Logo';

class UserList extends Component {
	constructor(props) {
		super(props);
		this.onEdit = this.onEdit.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onAdd = this.onAdd.bind(this);
	}

	componentDidMount() {
		this.props.fetchUsers();
	}

	onEdit(id) {
		this.props.history.push(`/user/${id}`);
	}

	onDelete(id) {
		this.props.deleteUser(id);
	}

	onAdd() {
		this.props.history.push('/user');
	}

	render() {
		return (
			<div className="wrapper">
				<Logo/>
				<div className="row">
					<div className="list-group col-12">
						{
							this.props.users.map(user => {
								return (
									<UserItem
										key={user.id}
										id={user.id}
										name={user.name}
										surname={user.surname}
										email={user.email}
										onEdit={this.onEdit}
										onDelete={this.onDelete}
									/>
								);
							})
						}
					</div>
					<div className="col-5">
						<button
							className="btn btn-success btn-lg"
							onClick={this.onAdd}
							style={{ margin: "10px" }}
						>
							Add user
						</button>
					</div>
				</div>
			</div>
		);
	}
}

UserList.propTypes = {
    userData: PropTypes.object
};

const mapStateToProps = (state) => {
	return {
		users: state.users
	}
};

const mapDispatchToProps = {
	...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);