export const SET_USER = 'SET_USER';
export const CHECK_CREDENTIALS = 'CHECK_CREDENTIALS';
export const LOGOUT = 'LOGOUT';