import {
    SET_USER
} from './actionTypes';

const initialState = {
    user: undefined,
    isAuthorized: false,
    isAdmin: false
}

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                user: action.user,
                isAuthorized: Boolean(action.user && action.user.id),
                isAdmin: Boolean(action.user && action.user.name === "admin")
            };
        default:
            return state;
    }
}