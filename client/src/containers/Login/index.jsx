import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { checkCredentials } from './actions';
import {  Redirect } from 'react-router';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
        }
    } 

    loginChanged = login => this.setState({ login });

    passwordChanged = password => this.setState({ password });

    handleLogin(ev) {
        ev.preventDefault();
        const { checkCredentials } = this.props;
        const { login, password } = this.state;
        checkCredentials({ login, password });
    }

    render() {
        return !this.props.isAuthorized
        ? (
            <div className="container">
                <div className="d-flex justify-content-center align-items-center" style={{ height: "100vh"}}>
                    <form onSubmit={ev => this.handleLogin(ev)}>
                        <div className="form-group">
                            <input 
                                type="name" 
                                className="form-control form-control-lg" 
                                placeholder="Enter login"
                                onChange={ev => this.loginChanged(ev.target.value) }
                            />
                        </div>
                        <div className="form-group">       
                            <input 
                                type="password" 
                                className="form-control form-control-lg" 
                                placeholder="Password"
                                onChange={ev => this.passwordChanged(ev.target.value) }
                            />
                        </div>
                        <div className="row">
                            <div className="col text-center">
                                <button type="submit" className="btn btn-primary btn-lg">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
        : <Redirect to='/'/>;
    }
}

Login.propTypes = {
    checkCredentials: PropTypes.func.isRequired,
    isAuthorized: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
    isAuthorized: state.auth.isAuthorized,
});

export default connect(mapStateToProps, { checkCredentials })(Login);