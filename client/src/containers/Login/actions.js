import {
    CHECK_CREDENTIALS,
    SET_USER,
    LOGOUT
} from './actionTypes';

export const checkCredentials = credentials => ({
    type: CHECK_CREDENTIALS,
    credentials
});

export const setUser = (user = undefined) => ({
    type: SET_USER,
    user
});

export const logout = () => ({
    type: LOGOUT
});