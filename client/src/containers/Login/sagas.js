import axios from 'axios';
import api from '../../shared/config/api';
import jwt from 'jsonwebtoken';
import { call, put, takeLatest, all } from 'redux-saga/effects';
import { CHECK_CREDENTIALS, LOGOUT } from './actionTypes';
import { setUser as actionSetUser } from './actions';

// Watcher saga -> actions -> worker saga

function* setUser(action) {
    try {
        const user = yield call(axios.post, `${api.url}/login`, action.credentials);
        const token = user.data.token;
        localStorage.setItem('jwt', token);
        yield put(actionSetUser(jwt.decode(token)));
    } catch (error) {
        yield put(actionSetUser());
        console.log('auth failed error:', error.message)
    }
}

function* watchCheckCredentials() {
    yield takeLatest(CHECK_CREDENTIALS, setUser);
}

function* logout() {
    localStorage.removeItem('jwt');
    yield put(actionSetUser());
}

function* watchLogout() {
    yield takeLatest(LOGOUT, logout);
}

export default function* loginSaga() {
    yield all([
        watchCheckCredentials(),
        watchLogout()
    ])
}