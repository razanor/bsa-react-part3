import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, access, ...rest }) => {
    return (
        <Route 
            {...rest} 
            render={props => {
                if (access) {
                    return <Component {...props}/>
                }
                else {
                    return <Redirect to={
                        {
                            pathname: '/',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                }
            }
        }/>
    )
}

export default PrivateRoute;