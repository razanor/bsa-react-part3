
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Login from './containers/Login';
import Chat from './containers/Chat';
import UserList from './containers/Users';
import UserPage from './containers/UserPage';
import { Switch, Route, Redirect } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';

const App = ({ isAuthorized, isAdmin}) => {
	return (
		<Switch>
			<Route exact path='/login' component={Login} />
			<Route exact path='/' render={() => ( 
				isAuthorized ? isAdmin ? <Redirect to='/users'/> : <Redirect to='/chat'/> 
				: <Redirect to='/login'/>
			 )}/>
			<PrivateRoute exact path="/user" component={UserPage} access={isAdmin} />
			<PrivateRoute path="/user/:id" component={UserPage} access={isAdmin} />
			<PrivateRoute exact path='/users' component={UserList} access={isAdmin} />
			<PrivateRoute exact path='/chat' component={Chat} access={isAuthorized} />
		</Switch>
	);
}

App.propTypes = {
	isAuthorized: PropTypes.bool.isRequired,
	isAdmin: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
    isAuthorized: state.auth.isAuthorized,
    isAdmin: state.auth.isAdmin
});


export default connect(mapStateToProps, null)(App);