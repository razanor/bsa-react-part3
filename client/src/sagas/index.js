import { all } from 'redux-saga/effects';
import chatSaga from '../containers/Chat/sagas';
import loginSaga from '../containers/Login/sagas'; 
import usersSaga from '../containers/Users/sagas';
import userPageSaga from '../containers/UserPage/sagas';

export default function* rootSaga() {
    yield all([
        chatSaga(),
        loginSaga(),
        usersSaga(),
        userPageSaga()
    ])
}