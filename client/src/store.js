import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import chatReducer from './containers/Chat/reducer';
import loginReducer from './containers/Login/reducer';
import usersReducer from './containers/Users/reducer';
import userPageReducer from './containers/UserPage/reducer';
import rootSaga from './sagas';
import { composeWithDevTools } from 'redux-devtools-extension';
import { setUser } from './containers/Login/actions';
import jwt from 'jsonwebtoken';

const initialState = {};

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const reducers = {
    messages: chatReducer,
    auth: loginReducer,
    users: usersReducer,
    userPage: userPageReducer
};

const rootReducer = combineReducers({
    ...reducers
});

const store = createStore(
    rootReducer, 
    initialState,
    composeWithDevTools(composedEnhancers)
);

if (localStorage.jwt) {
    store.dispatch(setUser(jwt.decode(localStorage.jwt)));
}

sagaMiddleware.run(rootSaga);

export default store;