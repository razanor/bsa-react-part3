import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.css';
import 'semantic-ui-css/semantic.min.css';
import './styles/index.css';

import store from './store';

const target = document.getElementById('root');
render(<Provider store={store}>
    <Router>
        <Route path="/" component={App} />
    </Router>
</Provider>, target);