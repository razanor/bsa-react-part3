const messageM = require('../models/message.model');


async function getMessages(req, res) {
    try {
        const messages = await messageM.allMessages();
        return res.json(messages);
    } catch (err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        }
    }
}

async function getMessage(req, res) {
    try {
        const id = req.params.id;
        const message = await messageM.messageById(id);
        return res.json(message);
    } catch (err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        } 
    }
}

async function createMessage(req, res) {
    try {
        const newMessage = await messageM.insertMessage(req.body);
        return res.status(201).json({
            message: `The message #${newMessage.id} has been created`,
            content: newMessage
        });
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function updateMessage(req, res) {
    try {
        const id = req.params.id;
        const updMessage = await messageM.updateMessage(id, req.body);
        return res.json({
            message: `The message #${id} has been updated`,
            content: updMessage
        });
    } catch(err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        } 
    }
}

async function deleteMessage(req, res) {
    try {
        const id = req.params.id;
        await messageM.deleteMessage(id);
        return res.json({
            message: `The message #${id} has been deleted`
        });
    } catch(err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        }   
    }
}

module.exports = {
    getMessages,
    getMessage,
    createMessage,
    updateMessage,
    deleteMessage
}