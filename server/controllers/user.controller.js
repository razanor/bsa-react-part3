const userM = require('../models/user.model');

async function getUsers(req, res) {
    try {
        const users = await userM.allUsers();
        return res.json(users);
    } catch (err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        }
    }
}

async function getUser(req, res) {
    try {
        const id = req.params.id;
        const user = await userM.userById(id);
        return res.json(user);
    } catch (err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        } 
    }
}

async function createUser(req, res) {
    try {
        const newUser = await userM.insertUser(req.body);
        return res.status(201).json({
            message: `The user #${newUser.id} has been created`,
            content: newUser
        });
    } catch(err) {
        res.status(500).json({ message: err.message });
    }
}

async function updateUser(req, res) {
    try {
        const id = req.params.id;
        const updUser = await userM.updateUser(id, req.body);
        return res.json({
            message: `The user #${id} has been updated`,
            content: updUser
        });
    } catch(err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        } 
    }
}

async function deleteUser(req, res) {
    try {
        const id = req.params.id;
        await userM.deleteUser(id);
        return res.json({
            message: `The user #${id} has been deleted`
        });
    } catch(err) {
        if (err.status) {
            res.status(err.status).json({ message: err.message });
        } else {
            res.status(500).json({ message: err.message });
        }   
    }
}

module.exports = {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser
}