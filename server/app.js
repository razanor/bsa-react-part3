/* Import packages */
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const passport = require('passport');

require('./passport.config');
passport.serializeUser((user, done) => { done(null, user); });
passport.deserializeUser((user, done) => { done(null, user); });

/* App init */
const app = express();

app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/users', require('./router/users.routes'));
app.use('/messages', require('./router/messages.routes'));
app.use('/', require('./router/auth.routes'));

/* Errors handling for not existing routes and server errors */
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;

