const fs = require('fs');

// Express-validator package
const { check } = require('express-validator/check');

const validationArray = [
    check('email')
        .isEmail()
        .withMessage('must be a valid email'),
    check('name')
        .isString( {min: 3} )
        .withMessage('Can\'t be less than 3'),
    check('surname')
        .isString( {min: 3} )
        .withMessage('Can\'t be less than 3'),
    check('password')
        .isString( {min: 3} )
        .withMessage('Can\'t be less than 3')
];

const mustExist = (arr, id) => {
    return new Promise((resolve, reject) => {
        const row = arr.find(r => r.id == id);
        if (!row) {
            reject({
                message: 'No data with this ID',
                status: 404
            });
        }
        resolve(row);
    });
}

const getNewId = (arr) => {
    if (arr.length > 0) {
        const newId = +(arr[arr.length - 1].id) + 1;
        return newId.toString();
    }
    return "1";
}

const writeJSONFile = (filename, content) => {
    fs.writeFile(filename, JSON.stringify(content), 'utf8', (err) => {
        if (err) {
            console.log(err);
        }
    });
}

module.exports = {
    mustExist,
    getNewId,
    writeJSONFile,
    validationArray
}