const express = require('express');
const router = express.Router();

const user = require('../controllers/user.controller');

/* All users */
router.get('/', user.getUsers);

/* Specific user by id */
router.get('/:id', user.getUser);

/* Create a new user */
router.post('/', user.createUser);

/* Update a user */
router.put('/:id', user.updateUser);

/* Delete a user */
router.delete('/:id', user.deleteUser);

module.exports = router;