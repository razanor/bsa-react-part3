const express = require('express');
const router = express.Router();

const message = require('../controllers/message.controller');

/* All messages */
router.get('/', message.getMessages);

/* Specific message by id */
router.get('/:id', message.getMessage);

/* Create a new message */
router.post('/', message.createMessage);

/* Update a message */
router.put('/:id', message.updateMessage);

/* Delete a message */
router.delete('/:id', message.deleteMessage);

module.exports = router;