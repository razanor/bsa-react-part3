const fs = require('fs');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

let users = {};
fs.readFile('./data/users.json', 'utf-8', (err, data) => {
if (err) throw err
    users = JSON.parse(data)
})

router.post('/login', (req, res, next) => {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.name === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
        const token = jwt.sign(userInDB, 'someSecret', { expiresIn: '24h' });
        res.status(200).json({ auth: true, token });
    } else {
        res.status(401).json({ auth: false });
    }
});

module.exports = router;