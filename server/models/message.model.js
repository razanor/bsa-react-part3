const fs = require('fs');
const helper = require('../helpers/helper.js');
const filename = './data/messages.json';

/* Read data from file asynchronously */
let messages = null;
fs.readFile(filename, (err, data) => {
    if (err) throw err;
    messages = JSON.parse(data);
});

const allMessages = () => {
    return new Promise((resolve, reject) => {
        if (messages.length == 0) {
            reject({
                message: 'no messages available',
                status: 202
            })
        }
        resolve(messages);
    });
}

const messageById = (id) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(messages, id)
        .then(message => resolve(message))
        .catch(err => reject(err))
    });
}

const insertMessage = (message) => {
    return new Promise((resolve, reject) => {
        const id = { id: helper.getNewId(messages) };
        const newMessage = { ...id, ...message };

        messages.push(newMessage);
        helper.writeJSONFile(filename, messages);
        resolve(newMessage);
    });
}

const updateMessage = (id, newData) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(messages, id)
        .then( message => {
            const index = messages.findIndex(m => m.id == message.id);
            messages[index] = { id: message.id, ...newData};
            helper.writeJSONFile(filename, messages);
            resolve(messages[index]);
        })
        .catch(err => reject(err));
    });
}

const deleteMessage = (id) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(messages, id)
        .then(() => {
            messages = messages.filter(m => m.id != id);
            helper.writeJSONFile(filename, messages);
            resolve();
        })
        .catch(err => reject(err));
    })
}

module.exports = {
    allMessages,
    messageById,
    insertMessage,
    updateMessage,
    deleteMessage
}