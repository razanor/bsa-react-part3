const fs = require('fs');
const helper = require('../helpers/helper.js');
const filename = './data/users.json';

/* Read data from file asynchronously */
let users = null;
fs.readFile(filename, (err, data) => {
    if (err) throw err;
    users = JSON.parse(data);
});

const allUsers = () => {
    return new Promise((resolve, reject) => {
        if (users.length == 0) {
            reject({
                message: 'no users available',
                status: 202
            })
        }
        resolve(users);
    });
}

const userById = (id) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(users, id)
        .then(user => resolve(user))
        .catch(err => reject(err))
    });
}

const insertUser = (user) => {
    return new Promise((resolve, reject) => {
        const id = { id: helper.getNewId(users) };
        const newUser = { ...id, ...user };

        users.push(newUser);
        helper.writeJSONFile(filename, users);
        resolve(newUser);
    });
}

const updateUser = (id, newData) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(users, id)
        .then( user => {
            const index = users.findIndex(u => u.id == user.id);
            users[index] = { id: user.id, ...newData};
            helper.writeJSONFile(filename, users);
            resolve(users[index]);
        })
        .catch(err => reject(err));
    });
}

const deleteUser = (id) => {
    return new Promise((resolve, reject) => {
        helper.mustExist(users, id)
        .then(() => {
            users = users.filter(u => u.id != id);
            helper.writeJSONFile(filename, users);
            resolve();
        })
        .catch(err => reject(err));
    })
}

module.exports = {
    allUsers,
    userById,
    insertUser,
    updateUser,
    deleteUser
}